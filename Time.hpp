#ifndef TIMER_H
# define TIMER_H

#include "Timer.class.hpp"
#include <ncurses.h>
#include <curses.h>
#include <iostream>


class Timer {

 public:

  Timer(void);
  Timer(clock_t t);
  Timer(Timer &obj);
  virtual ~Timer(void);
  Timer &operator=(Timer const &r);

  int  checkTime(clock_t t);
  int  getCurrentTime(clock_t t);

 private:
  clock_t _timer;
};

#endif