#ifndef MAP_HPP
# define MAP_HPP

#include "Enemy.hpp"
#include "Player.hpp"
#include "Bullet.hpp"
#include "Stars.hpp"
#include <curses.h>
#include <ncurses.h>
#include <fstream>
#include <unistd.h>

#define X 40
#define Y 120

extern std::ofstream g_fd;

class Map
{
private:
	Enemy **_enemy;
	Player	_player;
	Stars **_stars;
	Bullet**_bullets;
	bool	_end;
	WINDOW*	_win;
	WINDOW*	_menu;


public:
	Map(void);
	virtual ~Map(void);
	Map(Map const & src);
	Map	&operator=(Map const & src);
	void	game(void);
};

#endif
