#include "Player.hpp"

Player::Player(void)
{
	//_x = ???
	//_y = ???
	_lives = 3;
	// std::cout << "Default player was created" << std::endl;
}

Player::Player(int x, int y, int lives) : _x(x), _y(y), _lives(lives)
{
	// std::cout << "Player with " << _lives <<  " lives was created on cordinates x = " << _x << " y = " << _y << std::endl;
}

Player::Player(Player const & src)
{
	std::cout << "Player copy constructor" << std::endl;
	*this = src;
}

Player::~Player(void)
{
	// std::cout << "Player was killed" << std::endl;
}

void	Player::setX(int x)
{
	_x = x;
}

void	Player::setY(int y)
{
	_y = y;
}

void	Player::up(void)
{
	_x++;
}

void	Player::down(void)
{
	_x--;
}

void	Player::right(void)
{
	_y++;
}

void	Player::left(void)
{
	_y--;
}

int	Player::getX(void)
{
	return _x;
}

int	Player::getY(void)
{
	return _y;
}

bool	Player::take_live(void)
{
	_lives--;
	if (_lives > 0)
		return 1;
	return (0);
}

bool	Player::got_hit(int x, int y)
{
	if (_x == x && _y == y)
		return 1;
	return 0;
}

bool	Player::check_lives(void)
{
	static int check = 0;
	_lives--;
	if (_lives == 0 || (_lives <= -1 && check >=2))
	{
		check = 0;
		return 0;
	}
	if (_lives <= -1)
		check++;
	return 1;
}

Player	&Player::operator=(Player const & src)
{
	if (this != &src)
	{
		_x = src._x;
		_y = src._y;
		_lives = src._lives;
	}
	return *this;
}