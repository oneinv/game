#include "Stars.hpp"

Stars::Stars(void)
{
	// std::cout << "Default Stars was created" << std::endl;
}

Stars::Stars(int x, int y)
{
	_x = x;
	_y = y;
	// std::cout << "Default Stars was created" << std::endl;
}

Stars::Stars(Stars const & src)
{
	std::cout << "Stars copy constructor" << std::endl;
	*this = src;
}

Stars::~Stars(void)
{
	std::cout << "Stars was killed" << std::endl;
}

int	Stars::getX(void)
{
	return _x;
}

int	Stars::getY(void)
{
	return _y;
}

void	Stars::setY(int y)
{
	_y = y;
}

void	Stars::setX(int x)
{
	_x = x;
}


Stars	&Stars::operator=(Stars const & src)
{
	if (this != &src)
	{
		_x = src._x;
		_y = src._y;
		_lives = src._lives;
	}
	return *this;
}