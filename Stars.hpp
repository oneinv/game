#ifndef STARS_HPP
# define STARS_HPP

#include "Player.hpp"
#include <iostream>

class Stars : public Player
{
public:
	Stars(void);
	Stars(int x, int y);
	virtual ~Stars(void);
	Stars(Stars const & src);
	Stars &operator=(Stars const &src);
	int	getX(void);
	int	getY(void);
	void	setY(int y);
	void	setX(int x);
};

#endif