#ifndef PLAYER_HPP
# define PLAYER_HPP

#include <iostream>

class Player
{
protected:
	int	_x;
	int	_y;
	int	_lives;
public:
	Player(void);
	Player(int x, int y, int _hitpoints);
	virtual ~Player(void);
	Player(Player const & src);
	Player &operator=(Player const &src);

	int		getX(void);
	int		getY(void);
	virtual void	setX(int x);
	virtual void	setY(int y);
	void	left(void);
	void	right(void);
	void	up(void);
	void	down(void);
	bool	take_live(void);
	bool	got_hit(int x, int y);
	bool	check_lives(void);
};

#endif
