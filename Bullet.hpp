#ifndef BULLET_H
# define BULLET_H

#include <curses.h>
#include <ncurses.h>

class Bullet {

 public:

  WINDOW * win;
  Bullet(void);
  Bullet(int x, int y, WINDOW *win);
  Bullet(Bullet &obj);
  virtual ~Bullet(void);
  Bullet &operator=(Bullet const &r);

  int   getCoordX(void);
  int   getCoordY(void);

  int   getLife(void);
  void  setLife(int i);

  void  moveDown(void);
  void  moveUp(void);

  void  makeShot(int x, int y);

  void	clearBullet(void);


 private:

  int  _x;
  int  _y;
  int  _life;
  

};

#endif