#include "Enemy.hpp"

Enemy::Enemy(void)
{
	std::cout << "Default enemy was created" << std::endl;
}

Enemy::Enemy(int x, int y)
{
	_x = x;
	_y = y;
}

Enemy::Enemy(Enemy const & src)
{
	std::cout << "Enemy copy constructor" << std::endl;
	*this = src;
}

Enemy::~Enemy(void)
{
	std::cout << "Enemy was killed" << std::endl;
}

Enemy	&Enemy::operator=(Enemy const & src)
{
	if (this != &src)
	{
		_x = src._x;
		_y = src._y;
		_lives = src._lives;
	}
	return *this;
}