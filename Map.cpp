#include "Map.hpp"

#define BULLETS_NBR 100

Map::Map(void)
{
	_end = 1;
	_stars = new Stars*[50];
	_enemy = new Enemy*[10];
	_bullets = new Bullet*[BULLETS_NBR];
	for(int i = 0; i < BULLETS_NBR; i++)
		_bullets[i] = NULL;
	for(int i = 0; i < 50; i++)
		_stars[i] = new Stars(rand() % X, rand() % Y);
	for(int i = 0; i < 10; i++)
		_enemy[i] = new Enemy(rand() % 10 + 1, rand() % Y);
	initscr();
	curs_set(0);
	noecho();
	nodelay(stdscr, TRUE);
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	_win = newwin(X, Y, 0, 0);
	_menu = newwin(20, 50, 0, 121);
	g_fd << "Default map was created" << std::endl;
}

Map::~Map(void)
{
	delwin(_win);
	endwin();
	g_fd << "Map was deleted" << std::endl;
}

Map::Map(Map const & src)
{
	*this = src;
}

Map	&Map::operator=(Map const & src)
{
	if (this != &src)
	{

	}
	return *this;
}

void	Map::game(void)
{
	int ch;
	int timeinfo(time(NULL));
	srand(clock());
	_player.setX(38);
	_player.setY(60);
	while (true)
	{
		if (_end == 0)
			break;
		mvwprintw(_menu, 1, 15, "===GAME STARED===");
		mvwprintw(_menu, 2, 3, "lives = 1");
		mvwprintw(_menu, 3, 3, "level = 0");
		mvwprintw(_menu, 4, 3, "time = %d", time(NULL) - timeinfo);
		mvwprintw(_menu, 5, 17, "===HELP===");
		mvwprintw(_menu, 6, 3, "A -> move left");
		mvwprintw(_menu, 7, 3, "D -> move right");
		mvwprintw(_menu, 8, 3, "SPACE -> shoot");
		mvwprintw(_menu, 9, 3, "ESC -> exit game");
		
		unsigned int sec = 64000;
		usleep(sec);
		wclear(_win);
		wattron(_win, COLOR_PAIR(1));
		box(_win, '*', '*');
		box(_menu, '|', '*');
		wattroff(_win, COLOR_PAIR(1));
		for(int i = 0; i < 50; i++)
		{
			if (_stars[i]->getX() > X - 2)
				_stars[i]->setX(0);
			else
			{
				mvwaddstr(_win, _stars[i]->getX(), _stars[i]->getY(), ".");
			}
			_stars[i]->up();
		}
		wattron(_win, COLOR_PAIR(2));
		mvwaddstr(_win, _player.getX(), _player.getY(), "^");
		wattroff(_win, COLOR_PAIR(2));
		wattron(_win, COLOR_PAIR(2));
		for (int i = 0; i < 10; i++)
		{
			if (_enemy[i] && _enemy[i]->got_hit(_player.getX(), _player.getY())) 
				{
					_end = -1;
					return;
				}
		}
		for(int i = 0; i < 10; i++)
		{
			if (_enemy[i])
			{
				if (_enemy[i]->getX() > X - 2)
					_enemy[i]->setX(0);
				else
					mvwaddstr(_win, _enemy[i]->getX(), _enemy[i]->getY(), "@");
				_enemy[i]->up();
			}
		}
		wattroff(_win, COLOR_PAIR(2));
		ch = getch();
		if (_player.getY() > 2 && ch == 'a')
			_player.setY(_player.getY() - 1);
		else if (_player.getY() < Y - 2 && ch == 'd')
			_player.setY(_player.getY() + 1);
		if (ch == 32)
		{
			for (int i = 0; i < BULLETS_NBR; i++)
				if (_bullets[i] == NULL)
				{
					_bullets[i] = new Bullet(_player.getX(), _player.getY(), _win);
					break;
				}
		}

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < BULLETS_NBR; j++)
				if (_enemy[i] && _bullets[j] &&
					_enemy[i]->got_hit(_bullets[j]->getCoordX(), _bullets[j]->getCoordY())) 
				{
					delete _enemy[i];
					delete _bullets[j];
					_bullets[j] = NULL;
					_enemy[i] = NULL;
					break ;
				}
		}

		for (int i = 0; i < BULLETS_NBR; i++)
		{
			if (_bullets[i] && _bullets[i]->getCoordX() >= 0)
			{
				_bullets[i]->moveUp();
				_bullets[i]->makeShot(_bullets[i]->getCoordX(), _bullets[i]->getCoordY());
			}
			else if (_bullets[i])
			{
				delete _bullets[i];
				_bullets[i] = NULL;
			}
		}
		if (ch == 27)
			break ;
		wrefresh(_win);
		wrefresh(_menu);
	}
}