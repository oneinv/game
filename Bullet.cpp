#include "Bullet.hpp"

Bullet::Bullet(void)
{

}

Bullet::Bullet(int x, int y, WINDOW *win) : _x(x), _y(y)
{
	this->win = win;
}

Bullet::Bullet(Bullet &obj)
{
 *this = obj;
}

Bullet::~Bullet(void)
{

}

Bullet &Bullet::operator=(Bullet const &r)
{
 this->_x = r._x;
 this->_y = r._y;
 return (*this);
}

int   Bullet::getCoordX(void)
{
 return (this->_x);
}

int   Bullet::getCoordY(void)
{
 return (this->_y);
}

void  Bullet::moveDown(void)
{
 this->_y++;
}

void  Bullet::moveUp(void)
{
 this->_x--;
}

void  Bullet::makeShot(int x, int y)
{
	attron(COLOR_PAIR(1));
	mvwaddstr(win, x, y, "|");
	attroff(COLOR_PAIR(1));
}

void  Bullet::clearBullet(void)
{
	mvprintw(_y, _x, " ");
}
