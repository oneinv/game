NAME = ft_retro

SRC = main Enemy Map Player Stars Bullet

OBJ = $(addprefix obj/, $(addsuffix .o, $(SRC)))

HEADER = Enemy.hpp Map.hpp Player.hpp Stars.hpp Bullet.hpp

FLAGS =  -Wall -Wextra -Werror

all: $(NAME)

$(NAME): obj $(OBJ)
	clang++ $(OBJ) -o $(NAME) -lncurses

obj/%.o: %.cpp $(HEADER)
	clang++ $(FLAGS) -c $< -o $@

obj:
	@mkdir obj

clean:
	@rm -rf obj
	@echo "Objects have been deleted"

fclean: clean
	@rm -f $(NAME)
	@echo "$(NAME) have been deleted"

re: fclean all
