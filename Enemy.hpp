#ifndef ENEMY_HPP
# define ENEMY_HPP

#include "Player.hpp"

class Enemy : public Player
{
private:

public:
	Enemy(void);
	Enemy(int x, int y);
	virtual ~Enemy(void);
	Enemy(Enemy const & src);
	Enemy	&operator=(Enemy const  & src);

};

#endif
