#include "Timer.class.hpp"

Timer::Timer(void)
{

}

Timer::Timer(clock_t t) : _timer(t)
{

}

Timer::Timer(Timer &obj)
{
 *this = obj;
}

Timer::~Timer(void)
{

}

Timer &Timer::operator=(Timer const &r)
{
 this->_timer = r._timer;
 return (*this);
}

int  Timer::checkTime(clock_t t)
{
 return ((this->_timer - t));
}

int  Timer::getCurrentTime(clock_t t)
{
 return ((t - this->_timer));
}